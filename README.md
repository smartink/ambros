# README

This README documents whatever steps are necessary to get **Ambros Tools** installed on your development machine.

## What is Ambros for?

**Ambros** is a set of terminal tools that allow you to control and conveniently access different **web projects**
you're developing on your machine and running with **Docker**.

### Provided tools:
* **amp (AMbros Proxy):** Relying on the [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) that will
be automatically installed with Docker, the `amp` command allows you to start and stop your dockerized projects
and access their **web-services** using **virtual-host names** instead of the classical mess-up of ip addresses,
port numbers and so on.
* **Resources for you projects:** located under the `doc/` folder (explained later).

## Installation
Clone the project:
```shell
git clone https://gitlab.com/smartink/ambros.git
```
This document refers the **Ambros Project's folder** with `/path/to/ambros`.

## Install Docker Engine

If you have not already done, proceed first with the installation of **docker** and **docker-compose** following the
proper instructions for you operating system:

- https://docs.docker.com/engine/install/
    - For Ubuntu: https://docs.docker.com/engine/install/ubuntu/
- https://docs.docker.com/compose/install/

Don't forget:
```shell
sudo systemctl enable docker.service
sudo systemctl start docker.service
```

If you are on **Linux** do not forget these
useful **[Post installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/)**
in particular:

- [Manage Docker as non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
- [Start Docker on Boot](https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot)

## Adjust your shell profile

Add the following line to your `~/.bashrc`, this will give you rapid access to the commands provided by this project:

    export PATH="/path/to/ambros/bin:$PATH"

## Set up the provided tools

## amp (AMbros Proxy)

### Create your first Startup Profile
The `amp` command allows you to start and stop one or more **dockerized projects** on your machine.

Which projects and in what order they should be started (and stopped) should be defined in a **profile file**.
You can create as many profiles as you want.

To create a **profile file** do as follows:

```shell
cd /path/to/ambros/conf
cp amp-rc-profile.example.yml.example amp-rc-profile.<PROFILE_NAME>.yml
```
Replace `<PROFILE_NAME>` with the name you have chosen for the profile,
use only **letters**, **digits**, **hyphens** and **underscores**, avoid blank spaces or special characters.

Open the new **profile file** and edit it according to your needs.
The provided example is quite explanatory of how to compile the file, here as follows some clarifications:

* under `projects` define a node foreach project you want to start
* each node must have a **unique name** containing only **letters**, **digits**, **hyphens** and **underscores**
* projects are started in the **order** they appear in the file and stopped in the opposite
* **foreach project's node** the following properties must be given:
  * `enabled`: `[boolean]` Set it to `true` otherwise the project will be ignored
  * `folder`: `[string]` The project's root folder ex. `/path/to/project-ABCD`
  * `ssl`: `[array]` Optional list of one or more folders (relative to `folder`) containing **SSL certificates**
    * `- path/to/aaa/ssl`
    * `- path/to/bbb/ssl`
    * `- path/to/ccc/ssl`
  * `script`: `[string]` The docker-project's control command, normally something like `docker-compose ...`
  * `cmds`:
    * `boot`: `[string]` The command arg to pass to the `script` to **boot** the project
    * `halt`: `[string]` The command arg to pass to the `script` to **halt** the project
    * `status`: `[string]` The command arg to pass to the `script` to **get status infos** of the project

> **Profiles** allow you to configure multiple startup combinations, for example with `profile_1`
starting `project_A`, `project_B` and `project_C`, with `profile_2` starting only
`project_B` and `project_D` and so on.

### Personalize environment file
Ambros recognizes some **environment variables**. Read the file `.env.example` in the `/path/to/ambros` folder
to know what are these variables and their **default values**.

If you need to override one or more value, create your `.env` file in the same folder and set the values
in according to your needs.

### Configure your Docker-Projects properly
In order to be able to communicate with the **Nginx Reverse Proxy** that Ambros will set up for you,
the **dockerized web-projects** you want to proxy must be **properly configured**.

The **Docker Container** of each service you want to connect to the reverse-proxy must define
the **environment variable** `VIRTUAL_HOST` assigned with the name of the virtual-host you want to use to access
the service.

> For example if you want to access the web-server of a certain project with the address
> `http://project1.foo.lo` you need to set `VIRTUAL_HOST=project1.foo.lo` of the respective container.
> If the container is listening on a different port than the usual `80` (http-port) you have also to declare
> this port using the `VIRTUAL_PORT` directive.

`VIRTUAL_HOST` and `VIRTUAL_PORT` directives can be easily set in the `docker-compose.yml` of each project
or through the `-e`/`--env` arguments of the `docker` command.

For further and more updated information please read the official documentations:

* **jwilder nginx-proxy** official documentation: https://hub.docker.com/r/jwilder/nginx-proxy
* **docker-compose** documentation env: https://docs.docker.com/compose/compose-file/compose-file-v3/#environment

### Configure local name resolution

In order to properly access the **virtual-hosts**, you need to add an entry in your `/etc/hosts` file for each
virtual-host you want to use.

But don't worry, you can do it later, the command `amp` explained in the next paragraph will tell you what to write
in the `/etc/hosts` file ;-)

### Start the profile
To start the defined profile type:
```shell
amp boot <PROFILE_NAME>
```

> Replace `<PROFILE_NAME>` with the name of the profile you want to start.
> For example, type `amp boot foobar` to start the profile defined in the file `/path/to/ambros/conf/amp-rc-profile.foobar.yml`.

The output of this command will print:

* The starting **status** of each project defined in the profile
* The **lines** you have **to insert** in the `/etc/hosts` file
* Useful **tips** on how to reach to the started services

### Important notices:
* Don't start at the same time different profiles that have one or more projects in common
* Use **only** `amp` to halt/restart projects already started with `amp`
* Don't edit the profile file when the profile is running

### Control commands
`$ amp status <PROFILE_NAME>` Prints status of the given profile and `/etc/hosts` **hints**

`$ amp halt <PROFILE_NAME>` Halts the given profile

`$ amp boot <PROFILE_NAME>` Boots the given profile

`$ amp ls` List all available profiles

`$ amp` Without arguments prints the usage guide

## Resources for you projects

### `dcm` template
In the `doc/` folder there is a file named `dcm.example`.

It is a **template** that allows you to create a wrapper script for `docker-compose` that will simplify the control
of your dockerized projects. **General shortcuts** valid for every project are already provided out of the box,
**custom shortcuts** can be easily written by you following the guidelines in the template itself.

For further information, please read the comments in the `dcm.example` file.
